#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <clientprefs>
#include <retakes/generic.sp>

new Handle:g_hZeusPref = INVALID_HANDLE;
new bool:g_bZeusPref[MAXPLAYERS+1];
Handle menuhandle;

public Plugin myinfo = {
	name = "Zuesgive",
	author = "Somighten",
	description = "Give all players a zues on round start, !zeus to change",
	version = "0.2",
	url = "https://bitbucket.org/somighten-yo/zeusgive/src"
};
/* register command, cookie, and hook round start */
public void OnPluginStart()
{
	g_hZeusPref = RegClientCookie("zeusgive_zeuspref", "zeusgive zeuspref", CookieAccess_Protected);
	RegConsoleCmd( "sm_zeus", zeus);	
	HookEvent("round_start", Event_Start);
}
public void OnClientConnected(int client)
{
	/* Assume true until cookies cached */
	g_bZeusPref[client] = true;
}
public Action:Event_Start(Handle:event, const String:name[], bool:dontBroadcast)
{
	for(int i = 1; i < MaxClients; i++)
	{
		/* Only give zeus to valid clients with pref set */
		if(IsClientConnected(i) && IsClientInGame(i) && IsPlayerAlive(i) && g_bZeusPref[i])
		{
						
			GivePlayerItem(i, "weapon_taser");
		}
	}
}
/* create menu on !zeus command */
public Action zeus(int client, int args)
{
	zeusMenu(client)
	return Plugin_Handled;
}
public zeusMenu(client)
{
	Handle:menuhandle = CreateMenu(MenuCallBack);
	SetMenuTitle(menuhandle, "Do you want to receive a zeus?");
	AddMenuItem(menuhandle, "Yes", "Yes");
	AddMenuItem(menuhandle, "No", "No");
	SetMenuExitButton(menuhandle, true);
	DisplayMenu(menuhandle, client, 250);
}
/* set pref array value and cookie on action select */
public MenuCallBack(Handle:menuhandle, MenuAction:action, client, position)
{
	if(action == MenuAction_Select)
	{
		decl String:Item[20];
		GetMenuItem(menuhandle, position, Item, sizeof(Item));
		if(StrEqual(Item, "Yes") && (IsClientConnected(client) && IsClientInGame(client)))
		{
			g_bZeusPref[client] = true;
			SetCookieBool(client, g_hZeusPref, true);
		}
		if(StrEqual(Item, "No") && (IsClientConnected(client) && IsClientInGame(client)))
		{
			g_bZeusPref[client] = false;
			SetCookieBool(client, g_hZeusPref, false);			
		}
	}
	else if(action == MenuAction_End)
	{
		CloseHandle(menuhandle)
	}
}
/* set pref array to cookie value when cookies cached */
public OnClientCookiesCached(client)
{
	if(IsFakeClient(client))
		return;
	g_bZeusPref[client] = GetCookieBool(client, g_hZeusPref);
}
